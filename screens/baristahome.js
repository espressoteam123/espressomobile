/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View} from 'react-native';


type Props = {};
export default class App extends Component<Props> {
  render() {
    return (
       <Text style={styles.buttonText}>Barista Home</Text>
    );
  }
}

const styles = StyleSheet.create({
                                 
 buttonText: {
    fontSize: 18,
    color: '#fff',
    alignSelf:'center',
    fontWeight: 'bold',
  }
                                 
});
