import React, { Component } from 'react';
import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
} from 'react-native';

import { Icon } from 'react-native-elements';

type Props = {};
export default class MenuItem extends Component<Props> {
    render() {
        return (
            <View key={this.props.keyval} style={styles.Menu}>
                <View style={styles.ItemIcon}>
                <Icon
                name='coffee'
                type='font-awesome'
                color='#343747' size={35} underlayColor={'transparent'}/>
                </View>
                <Text style={styles.ItemText}>{this.props.val.itemName}</Text>
                <Text style={styles.ItemText2}>{this.props.val.price}</Text>
                <Text style={styles.ItemText}>{this.props.val.description}</Text>
                <View style={styles.ItemDelete}>
                <Icon
                name='times-circle'
                type='font-awesome'
                color='#343747' size={35} underlayColor={'transparent'}
                onPress={this.props.deleteMethod} />
                </View>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    Menu: {
        position: 'relative',
        paddingTop: 20,
        paddingBottom: 10,
        paddingRight: 50,
        paddingLeft: 40,
        borderBottomWidth:2,
        width: 350,
        borderBottomColor: '#343747',
        color: '#343747',
        fontSize: 19,
        fontWeight: '500'
    },
    ItemText: {
        paddingLeft: 10,
        borderLeftWidth: 10,
        borderLeftColor: '#E91E63',
        fontSize: 19,
        color: '#343747',
        fontWeight: '500'
    },
    ItemText2: {
        position: 'absolute',
        paddingRight: 8,
        borderRightWidth: 10,
        borderRightColor: '#E91E63',
        top: 20,
        bottom: 20,
        right: 40,
        fontSize: 19,
        color: '#343747',
        fontWeight: '500'
    },
    ItemDelete: {
        position: 'absolute',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'transparent',
        padding: 10,
        top: 15,
        right: 10
    },
    ItemIcon: {
        position: 'absolute',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'transparent',
        padding: 10,
        top: 15,
        
    },
    ItemDeleteText: {
        color: 'white'
    }
});
