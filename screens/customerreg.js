/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, KeyboardAvoidingView, ImageBackground, Image, TouchableHighlight, TextInput, ActivityIndicator} from 'react-native';
import { Icon } from 'react-native-elements';
import t from 'tcomb-form-native';
import {writeNewUser} from '../schema/espresso_lib';

const Form = t.form.Form;


const Email = t.refinement(t.String, email => {
  const reg = /^[-\w\.\$@\*\!]{5,50}/; //check for valid email
  return reg.test(email);
});

const Password = t.refinement(t.String, (str) => {
  return (str.length >= 5 && str.length <= 50); // minimum password length should be 5 symbols
});

const Username = t.refinement(t.String, (str) => { // will also check availability later
  return (str.length >= 3 && str.length <= 50); // minimum username length should be 3 symbols
});

const User = t.struct({
  email: Email,
  username: Username,
  password: Password,
  confirm_password: t.String,
});

const formStyles = {
  ...Form.stylesheet,
  controlLabel: {
    normal: {
      color: '#343747',
      fontSize: 17,
      marginBottom: 2,
      fontWeight: '600'
    },
    error: {
      color: '#343747',
      fontSize: 17,
      marginBottom: 2,
      fontWeight: '600'
    },
  },
  errorBlock: {
      fontSize: 14,
      marginBottom: 0,
      color: '#a94442',
  },
  textbox: {
    normal: {
      color: '#343747',
      fontSize: 17,
      height: 36,
      paddingVertical: Platform.OS === "ios" ? 7 : 0,
      paddingHorizontal: 7,
      borderRadius: 4,
      borderColor: '#343747',
      borderWidth: 1,
      marginBottom: 2
    },
    error: {
      color: '#343747',
      fontSize: 17,
      height: 36,
      paddingVertical: Platform.OS === "ios" ? 7 : 0,
      paddingHorizontal: 7,
      borderRadius: 4,
      borderColor: '#a94442',
      borderWidth: 1,
      marginBottom: 2
    },
  }
}



type Props = {};
export default class App extends Component<Props> {
  static navigationOptions = { title: 'Welcome', header: null };
  constructor () {
    super()
    this.state = {
      value: null,
      pwerror: false,
      unerror: false,
      processing: false,
      unerrortxt: 'Please create a valid username!',
    }
  };
  update(value){
      this.setState({value: value});
  }
  handleSubmit = () => {
      const value = this.refs.form.getValue();
      if (value){
          this.setState({processing: true})
          console.log('value: ', value);
          if (value.password == value.confirm_password){
              const newUser = {
                    name: '',
                    username: value.username,
                    password: value.password,
                    email: value.email,
                    dob: '01/01/01',
                    userType: 'customer',
              }
              writeNewUser(newUser)
                .then((user) => {
                    this.props.navigation.navigate('LoginScreen', {newaccount: true})
                }).catch((error) => {
                    this.setState({processing: false}); this.setState({unerror: true, unerrortxt: 'Username taken!'});
                })
          } else {
              this.setState({pwerror: true});
              this.setState({processing: false})
          }
        } else {
           this.setState({processing: false})
        }
      
  };
  render() {
    var options = {
      fields: {
        email: {
          error: 'Please provide a valid email address!',
        },
        username: {
          error: this.state.unerrortxt,
          hasError: this.state.unerror,
        },
        password: {
          error: 'Please create a password!',
          secureTextEntry: true,
        },
        confirm_password: {
          label: 'Confirm Password',
          hasError: this.state.pwerror,
          error: 'Password does not match above!',
          secureTextEntry: true,
        },
      },
      stylesheet: formStyles,
    };
    return (
     <KeyboardAvoidingView behavior='padding' style={styles.wrapper}>
          <ImageBackground style={styles.bgcontainer} source={require('../app/img/bg.jpg')}>
            <View style={styles.back}>
            <Icon
            name='arrow-circle-left'
            type='font-awesome'
            color='#343747' size={40} underlayColor={'transparent'}
            onPress={() => this.props.navigation.navigate('HomeScreen')} />
            </View>
            <Text style={styles.header}>EXPRESSO</Text>
            <Image style={styles.logo} source={require('../app/img/logo.png')}>
            </Image>
            <Text style={styles.header2}>SIGN UP AS A CUSTOMER</Text>
            <View style={styles.container}>
            <Form ref="form" type={User} onChange={this.update.bind(this)} value={this.state.value} options={options}/>
            <ActivityIndicator animating={this.state.processing} size="small" color="#343747" />
            </View>
            <TouchableHighlight style={styles.button}  onPress={this.handleSubmit.bind(this)}>
            <Text style={styles.buttonText}>CREATE ACCOUNT</Text>
            </TouchableHighlight>
          </ImageBackground>
      </KeyboardAvoidingView>
    );
  }
}

const styles = StyleSheet.create({
                                 
wrapper: {
    flex: 1,
  },
  bgcontainer: {
    flex: 1,
    alignSelf: 'stretch',
    width: null,
    justifyContent: 'center',
    alignItems: 'center',
  },
  container: {
    justifyContent: 'center',
    marginTop: 10,
    padding: 10,
    width: 285,
    color: '#343747',
  },
  logo:{
    width: 68,
    height: 106,
  },
  header: {
    fontSize: 38,
    color: '#343747',
    fontWeight: 'bold',
    fontFamily: 'Helvetica',
  },
  header2: {
    marginTop: 10,
    fontSize: 24,
    color: '#343747',
    fontWeight: 'bold',
    justifyContent: 'center',
    alignItems: 'center',
    fontFamily: 'Helvetica',
  },
  buttonText: {
    fontSize: 19,
    color: '#343747',
    alignSelf:'center',
    fontWeight: 'bold',
    fontFamily: 'Helvetica',
  },
  button: {
    height:55,
    borderWidth:2,
    backgroundColor:'rgba(189, 172, 162, 0.2)',
    borderColor:'#343747',
    width:270,
    justifyContent:'center',
    borderRadius: 9,
   },
  landoptions: {
    marginTop: 13,
    justifyContent: 'space-between',
    alignSelf: 'center',
    flex:0.3,
  },
  textinput:{
    alignSelf: 'stretch',
    height: 35,
    marginTop: 13,
    width: 250,
    color: '#343747',
    borderBottomColor: '#343747',
    borderBottomWidth: 1,
    fontSize: 19,
    fontFamily: 'Helvetica',
  },
  formoptions: {
    marginTop: 13,
    justifyContent: 'space-between',
    alignSelf: 'center',
    flex:0.5,
    marginBottom: 36,
  },
  back: {
    position: 'absolute',
    left: 10,
    top:20,
  },
  error: {
    backgroundColor:'rgba(255, 0, 0, 0.5)'
  },
  errorMessage: {
    color: 'red',
    fontSize: 14,
    fontFamily: 'Helvetica',
                                 
  }
                                 
});
