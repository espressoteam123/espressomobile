React Native Application

Requirements:
Npm 5.10
Node 8.3 
react-native command
watchman
Android SDK 23, Android Emulator 6.0
Xcode

Instructions:
for android:
create an android emulator in android studio (install that) make sure you use Android SDK 23 and Emulator 6.0
start the emulator from the command line:
emulator -list-avds
emulator -avd (whatever last command returned)

In a seperate terminal:
react-native link
react-native run-android


for apple: 
install xcode from app store

run these commands in project folder: 
react-native link
react-native run-ios
