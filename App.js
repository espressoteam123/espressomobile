/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, KeyboardAvoidingView, ImageBackground, Image, TouchableHighlight} from 'react-native';
//import {insertNewUser} from './schema';
import { StackNavigator } from 'react-navigation';
import Home from './screens/home';
import Login from './screens/login';
import CustReg from './screens/customerreg';
import BarReg from './screens/baristareg';
import CustOrder from './screens/custordcoffee';
import BarCoffMen from './screens/baristacoffeemenu'
import BarHome from './screens/baristahome';

const AppNavigator = StackNavigator({
   HomeScreen: { screen: Home },
   LoginScreen: { screen: Login },
   BaristaRegScreen: { screen: BarReg },
   CustomerRegScreen: { screen: CustReg },
   CustomerOrdScreen: { screen: CustOrder },
   BaristaCoffMenuScreen: { screen: BarCoffMen},
   BaristaHomeScreen : { screen: BarHome}
});



export default AppNavigator;


