import Realm from 'realm';
import {User, details, 
        Barista, Menu, MenuItem, CustomerOrder} from './schema';

const createRealm = (schema) => {
    return Realm.Sync.User.login(details.serverUrl, details.username, details.password)
        .then((user) => {
            let config = user.createConfiguration();
            console.log(config);
            config.schema = schema;
            return Realm.open(config)
        }).catch((error) => {
            return Promise.reject(error);
        })
}

/*
 * 
 * writeSet : [writes]
 * writes : 
 *  {
 *      "schema": Schema,
 *      "data": Data
 *  
 *  }
 *  writes a list of writes into the given schemas.
 */
const writeRealm = (writeSet, schema) => new Promise((resolve, reject) => {
    createRealm(schema)
        .then((realm) => {
            writeSet.forEach((write) => {
                console.log("i am writing....")
                console.log(write);
                realm.write(() => {
                    realm.create(write.schema, write.data);
                })
            })
            realm.close();
            resolve(writeSet);
        }).catch((error) => {
            reject(error);
        })
})

// queries a particular realm given 1 schema, and a query
// schema : [schema] array of size 1
// query : string
// schemaName : string
const queryRealm = (schema, query, schemaName) => new Promise((resolve, reject) => {
    createRealm(schema)
    .then((realm) => {
        let data = null;
        if (query){
            data = realm.objects(schemaName).filtered(query)
        } else {
            data = realm.objects(schemaName);
        }
        let subscription = data.subscribe();
        subscription.addListener((sub, state) => {
            switch (state) {
            case Realm.Sync.SubscriptionState.Complete:
                resolve(data);
                break;
            case Realm.Sync.SubscriptionState.Error:
                reject(subscription.error)
                break;
            }
        })
    }).catch((error) => {
        // console.log(error)
        reject(error);
    })
})


export const writeNewUser = (details) => {
    let writeSet = [{
        schema: User.name,
        data: details,
    }]
    let schema = [User];
    return checkUserByName(details.username)
        .then((user) => {
            if (user.isEmpty()){
                return writeRealm(writeSet, schema);
            } else {
                return Promise.reject("User Exists");
            }
        }).catch((error) => {
            return Promise.reject(error);
        })
}

export const writeNewBarista = (newUser, newMenu, name, address) => {
    const barista = {
        username: newUser.username,
        menu: newMenu,
        storeName: name,
        address: address,
    }
    let writeSet = [];
    writeSet.push({schema: Barista.name, data: barista});
    let schema = [Menu, MenuItem, Barista];
    return writeNewUser(newUser)
    .then((user) => {
        console.log(user);
        return writeRealm(writeSet, schema);
    }).catch((error) => {
        return Promise.reject(error);
    })
}

export const getUserByNamePassword = (username, password) => {
    let query = `username == "${username}" AND password == "${password}"`
    console.log(query);
    let schema = [User];
    return queryRealm(schema, query, User.name);
}

export const checkUserByName = (username) => {
    let query = `username == "${username}"`
    let schema = [User];
    return queryRealm(schema, query, User.name);
}

export const getAllBaristas = () => {
    return queryRealm([Barista, Menu, MenuItem], '', Barista.name);
}

export const getMenuForBarista = (username) => {
    let schema = [Barista, Menu, MenuItem];
    let query = `username == "${username}"`;
    return queryRealm(schema, query, Barista.name)
    .then((barista) => {
        let menu = barista['0'].menu;
        return Promise.resolve(menu);
    }).catch((error) => {
        return Promise.reject(error);
    })
}

export const createNewOrder = (customer, barista, menuOrders) => {
    let schema = [CustomerOrder];
    let newOrder = {
        orderId: '#12',
        menuOrder: menuOrders,
        customerUsername: customer,
        baristaUsername: barista,
        orderStatus: 'Receieved',  // Received -> In Progress -> Complete
        time: new Date(),
    }
    let writeSet = [{schema: CustomerOrder.name, data: newOrder}];
    return writeRealm(writeSet, schema);
}

const updateOrder = (realm, order, status) => new Promise((resolve, reject) => {
    console.log(order.orderStatus);
    realm.write(() => {
        order.orderStatus = status; 
    });
    // console.log(order);
    resolve(order);
})

export const getOrdersBarista = (barista) => {
    let schema = [CustomerOrder];
    let query = `baristaUsername == "${barista}" AND orderStatus != "Complete"`;
    return queryRealm(schema, query, CustomerOrder.name);
}

export const changeOrderStatus = (orderId, status) => {
    if (status != 'In Progress' && status != "Complete"){
        return Promise.reject();
    }
    let query = `orderId == "${orderId}"`;
    let schema = [CustomerOrder];
    return createRealm(schema)
    .then((realm) => {
        let data = null;
        // if (query){
        data = realm.objects(CustomerOrder.name).filtered(query);
        let subscription = data.subscribe();
        subscription.addListener((sub, state) => {
            switch (state) {
            case Realm.Sync.SubscriptionState.Complete:
                // resolve(data);
                return updateOrder(realm, data['0'], status);
            case Realm.Sync.SubscriptionState.Error:
                return Promise.reject("broken");
            }
        })
    }).catch((error) => {
        return Promise.reject(error);
    })
}

