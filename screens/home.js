/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, KeyboardAvoidingView, ImageBackground, Image, TouchableHighlight} from 'react-native';


export default class HomeScreen extends Component<Props> {
  static navigationOptions = { title: 'Welcome', header: null };
  render() {
    return (
     <KeyboardAvoidingView behavior='padding' style={styles.wrapper}>
          <ImageBackground style={styles.bgcontainer} source={require('../app/img/bg.jpg')}>
            <Text style={styles.header}>EXPRESSO</Text>
            <Image style={styles.logo} source={require('../app/img/logo.png')}>
            </Image>
            <View style={styles.landoptions}>
            <TouchableHighlight style={styles.button} underlayColor={'#39405C'} onPress={() => this.props.navigation.navigate('LoginScreen', {newaccount: false})}>
            <Text style={styles.buttonText}>LOGIN</Text>
            </TouchableHighlight>
            <TouchableHighlight style={styles.button} underlayColor={'#39405C'} onPress={() => this.props.navigation.navigate('CustomerRegScreen')}>
            <Text style={styles.buttonText}>SIGN UP AS CUSTOMER</Text>
            </TouchableHighlight>
            <TouchableHighlight style={styles.button} underlayColor={'#39405C'} onPress={() => this.props.navigation.navigate('BaristaRegScreen')}>
            <Text style={styles.buttonText}>SIGN UP AS BARISTA</Text>
            </TouchableHighlight>
            </View>
          </ImageBackground>
      </KeyboardAvoidingView>
    );
  }
}

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
  },
  bgcontainer: {
    flex: 1,
    alignSelf: 'stretch',
    width: null,
    justifyContent: 'center',
    alignItems: 'center',
  },
  logo:{
    width: 88,
    height: 126,
    marginBottom: 10,
        
  },
  header: {
    fontSize: 38,
    color: '#343747',
    fontWeight: 'bold',
    marginBottom: 10,
    fontFamily: 'Helvetica',
  },
  buttonText: {
    fontSize: 19,
    color: '#343747',
    alignSelf:'center',
    fontWeight: 'bold',
    fontFamily: 'Helvetica',
  },
  button: {
    height:55,
    borderWidth:2,
    backgroundColor:'rgba(189, 172, 162, 0.2)',
    borderColor:'#343747',
    width:270,
    justifyContent:'center',
    borderRadius: 9,
   },
  landoptions: {
    justifyContent: 'space-between',
    flex:0.39,
  }
                                 
});
