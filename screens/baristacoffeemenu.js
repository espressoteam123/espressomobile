import React, { Component } from 'react';
import {
    View,
    Text,
    StyleSheet,
    TextInput,
    ScrollView,
    TouchableOpacity,
    ImageBackground,
    TouchableHighlight,
    Platform,
    ActivityIndicator,
} from 'react-native';
import { Icon } from 'react-native-elements';
import Modal from 'react-native-modal';
import t from 'tcomb-form-native';
import MenuItem from '../app/components/menuitem';
import {writeNewBarista} from '../schema/espresso_lib';

const Form = t.form.Form;

const menuItem = t.struct({
      Name: t.String,
      Price: t.Number,
      Description: t.String
});

const formStyles = {
  ...Form.stylesheet,
  controlLabel: {
    normal: {
      color: '#CFC7C4',
      fontSize: 17,
      marginBottom: 2,
      fontWeight: '600'
    },
    error: {
      color: '#CFC7C4',
      fontSize: 17,
      marginBottom: 2,
      fontWeight: '600'
    },
  },
  errorBlock: {
      fontSize: 14,
      marginBottom: 0,
      color: '#a94442',
  },
  textbox: {
    normal: {
      color: '#CFC7C4',
      fontSize: 17,
      height: 36,
      paddingVertical: Platform.OS === "ios" ? 7 : 0,
      paddingHorizontal: 7,
      borderRadius: 4,
      borderColor: 'white',
      borderWidth: 1,
      marginBottom: 2,
      width: 280
    },
    error: {
      color: '#CFC7C4',
      fontSize: 17,
      height: 36,
      paddingVertical: Platform.OS === "ios" ? 7 : 0,
      paddingHorizontal: 7,
      borderRadius: 4,
      borderColor: '#a94442',
      borderWidth: 1,
      marginBottom: 2,
    width: 280
    },
  }
}

type Props = {};
export default class Main extends Component<Props> {
    static navigationOptions = { title: 'MenuCreate', header: null };
    state = {
    isModalVisible: false
    };
    _toggleModal = () => {
        this.setState({ isModalVisible: !this.state.isModalVisible})
        this.setState({value: null})
    };
    constructor(props){
        super(props);
        this.state = {
            menuArray: [],
            value: null,
            processing: false,
            helpertxt: (this.state.menuArray && this.state.menuArray.length > 0) ? '' : 'NO ITEMS ON YOUR MENU YET!'
            
        };
    }
    update(value){
        this.setState({value: value});
    }
    handleReg() {
        userdeets = this.props.navigation.state.params.user
         this.setState({processing: true})
        const newUser = {
            name: '',
            username: userdeets.username,
            password: userdeets.password,
            email: userdeets.email,
            dob: '01/01/01',
            userType: 'barista',
        }
        const newMenu = {
            menuList: this.state.menuArray,
            owner: newUser,
        }
        writeNewBarista(newUser, newMenu)
        .then((createdUser) => {this.props.navigation.navigate('LoginScreen', {newaccount: true});  this.setState({processing: false})})
        .catch((error) => {this.props.navigation.navigate('HomeScreen');  this.setState({processing: false})});
        
    }
    handleSubmit = () => {
      const value = this.refs.form.getValue();
      if (value){
          var d = new Date();
            this.state.menuArray.push({
                itemName: value.Name,
                price: parseFloat(value.Price),
                description: value.Description
            });
          this.setState({ isModalVisible: !this.state.isModalVisible });
          this.setState({ menuArray: this.state.menuArray });
          this.setState({helpertxt: (this.state.menuArray && this.state.menuArray.length > 0) ? '' : 'NO ITEMS ON YOUR MENU YET!'});
      }
    };
    render() {
        var options = {
          fields: {
            Name: {
              error: 'Please enter a name!',
            },
            Price: {
              error: 'Please enter a number!'
            },
            Description: {
              error: 'Please enter a description!',
            },
          },
          stylesheet: formStyles,
        };
        let items = this.state.menuArray.map((val, key)=>{
            return <MenuItem key={key} keyval={key} val={val}
                    deleteMethod={()=>this.deleteItem(key)}/>
        });
        return (
            <ImageBackground style={styles.bgcontainer} source={require('../app/img/bg.jpg')}>
                <View style={styles.back}>
                <Icon
                name='arrow-circle-left'
                type='font-awesome'
                color='#343747' size={35} underlayColor={'transparent'}
                onPress={() => this.props.navigation.navigate('BaristaRegScreen')} />
                </View>
                <Text style={styles.header}>CREATE YOUR MENU</Text>
                <Text style={styles.header3}>{this.state.helpertxt}</Text>
                <ScrollView style={styles.scrollContainer}>
                    {items}
                </ScrollView>
                <View style={styles.addButton}>
                <Icon
                name='plus-circle'
                type='font-awesome'
                color='#343747' size={70} underlayColor={'transparent'}
                onPress={this._toggleModal} />
                <ActivityIndicator animating={this.state.processing} size="small" color="#343747" />
                <TouchableHighlight disabled={(this.state.menuArray.length > 0) ? false : true} style={styles.button2}  onPress={this.handleReg.bind(this)}>
                <Text style={styles.buttonText2}>CREATE ACCOUNT</Text>
                </TouchableHighlight>
                </View>
                
                <View style={{ flex: 1 }}>
                <Modal backdropOpacity={0.95}  isVisible={this.state.isModalVisible}>
                      <View style={styles.container}>
                        <Text style={styles.header2}>NEW MENU ITEM</Text>
                        <View style={styles.container}>
                        <Form ref="form" type={menuItem} onChange={this.update.bind(this)} value={this.state.value} options={options}/>
                        </View>
                        <TouchableHighlight style={styles.button}  onPress={this.handleSubmit.bind(this)}>
                            <Text style={styles.buttonText}>ADD ITEM</Text>
                        </TouchableHighlight>
                        <TouchableHighlight style={styles.button}  onPress={this._toggleModal}>
                        <Text style={styles.buttonText}>CANCEL</Text>
                        </TouchableHighlight>
                      </View>
                    </Modal>
                  </View>
            </ImageBackground>
        );
    }
    deleteItem(key){
        this.state.menuArray.splice(key, 1);
        this.setState({menuArray: this.state.menuArray});
        this.setState({helpertxt: (this.state.menuArray && this.state.menuArray.length > 0) ? '' : 'NO ITEMS ON YOUR MENU YET!'});
    }
}
const styles = StyleSheet.create({
    bgcontainer: {
        flex: 1,
        alignSelf: 'stretch',
        width: null,
        justifyContent: 'center',
        alignItems: 'center',
    },
    back: {
        position: 'absolute',
        left: 10,
        top: 20,
        marginTop: 2
    },
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 10,
        padding: 10,
        color: 'white',
        width: null,
    },
    header: {
        fontSize: 30,
        color: '#343747',
        fontWeight: 'bold',
        fontFamily: 'Helvetica',
        justifyContent: 'center',
        marginTop: 25,
        marginLeft: 30
                            
    },
    header2: {
        marginTop: 10,
        fontSize: 24,
        color: '#CFC7C4',
        fontWeight: 'bold',
        justifyContent: 'center',
        alignItems: 'center',
        fontFamily: 'Helvetica',
    },
    header3: {
        marginLeft: 10,
        marginRight: 10,
        fontSize: 24,
        color: '#343747',
        fontWeight: 'bold',
        justifyContent: 'center',
        alignItems: 'center',
        textAlign: 'center',
        fontFamily: 'Helvetica',
        position: 'absolute',
    },
    headerText: {
        color: 'white',
        fontSize: 18,
        padding: 26
    },
    scrollContainer: {
        marginBottom: 130,
    },
    footer: {
        position: 'absolute',
        bottom: 0,
        left: 0,
        right: 0,
        zIndex: 10
    },
    textInput: {
        alignSelf: 'stretch',
        color: '#fff',
        padding: 20,
        backgroundColor: '#252525',
        borderTopWidth:2,
        borderTopColor: '#ededed'
    },
    addButton: {
        position: 'absolute',
        zIndex: 11,
        
        bottom: 50,
        backgroundColor: 'transparent',
        width: 70,
        height: 70,
        borderRadius: 35,
        alignItems: 'center',
        justifyContent: 'center',
        elevation: 8
    },
    addButtonText: {
        color: 'white',
        fontSize: 24
    },
    buttonText: {
        fontSize: 19,
        color: '#CFC7C4',
        alignSelf:'center',
        fontWeight: 'bold',
        fontFamily: 'Helvetica',
    },
    button: {
        height:55,
        borderWidth:2,
        backgroundColor:'rgba(189, 172, 162, 0.1)',
        borderColor:'#CFC7C4',
        width:270,
        justifyContent:'center',
        borderRadius: 9,
        marginBottom: 10,
    },
    buttonText2: {
        fontSize: 19,
        color: '#343747',
        alignSelf:'center',
        fontWeight: 'bold',
        fontFamily: 'Helvetica',
    },
    button2: {
        height:55,
        borderWidth:2,
        backgroundColor:'rgba(189, 172, 162, 0.1)',
        borderColor:'#343747',
        width:270,
        justifyContent:'center',
        borderRadius: 9,
        marginBottom: 10,
        marginTop: 10,
    },
});
