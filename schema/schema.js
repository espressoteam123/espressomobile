// import Realm from 'realm';

export const details =  {
    username: "admin",
    password: "password",
    serverUrl: "https://espresso.us1.cloud.realm.io"
};

export const USER_SCHEMA = "User";
export const MENU_SCHEMA = "Menu";
export const BARISTA_SCHEMA = "CoffeeBarista";
export const MENUITEM_SCHEMA = "MenuItem";
export const ORDER_SCHEMA = "CustomerOrders";

export const User = {
    name: USER_SCHEMA,
    properties: {
        name: 'string',
        username: 'string',
        password: 'string',
        email: 'string',
        dob: 'date',
        userType: 'string'
    }
}

export const MenuItem = {
    name: MENUITEM_SCHEMA,
    properties: {
        itemName: 'string',
        price: 'double',
        description: 'string',
    }
}

export const Menu = {
    name: MENU_SCHEMA,
    properties: {
        menuList: {type: 'list', objectType: MENUITEM_SCHEMA},
    }
}

export const Barista = {
    name: BARISTA_SCHEMA,
    properties: {
        username: 'string',
        menu: MENU_SCHEMA,
        storeName: 'string',
        address: 'string',
    }
}

export const CustomerOrder = {
    name: ORDER_SCHEMA,
    primaryKey: 'orderId',
    properties: {
        orderId: 'string',
        menuOrder: {type: 'list', objectType: 'string'},
        customerUsername: 'string',
        baristaUsername: 'string',
        orderStatus: 'string',  // Received -> In Progress -> Complete
        time: 'date'
    }
}